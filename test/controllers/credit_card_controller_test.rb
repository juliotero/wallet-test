require 'test_helper'

class CreditCardControllerTest < ActionController::TestCase
  test "should get number:string" do
    get :number:string
    assert_response :success
  end

  test "should get cvv:integer" do
    get :cvv:integer
    assert_response :success
  end

  test "should get expiration_date:date" do
    get :expiration_date:date
    assert_response :success
  end

end
