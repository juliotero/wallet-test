## Wallet
This wallet consists in adding N Credit card numbers to a list. 
The actions are:
- Click on add a new card, it'll show 3 empty fields (to enter the Credit card number,CVV and expiration date) and a save button. The new number should be stored when the user click on the "Save" button.
- Each credit card on the list will have an X, in order to remove an item when it's needed.
- It's not possible to edit the items. 

### Installation
1. Clone this repository to your computer.
2. Run ```bundle install```.
3. Run ```rake db:create```.
4. Run ```rake db:schema:load```.
