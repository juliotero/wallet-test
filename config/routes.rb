Rails.application.routes.draw do


  resources :credit_cards
  get '/wallet' => 'credit_cards#index'
  root :to => redirect('/wallet')

end
