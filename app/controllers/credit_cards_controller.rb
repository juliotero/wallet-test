class CreditCardsController < ApplicationController
  before_action :set_credit_card, only: [:destroy]

  # GET /credit_cards
  # GET /credit_cards.json
  def index
    @credit_cards = CreditCard.all
  end

  # GET /credit_cards/new
  def new
    @credit_card = CreditCard.new
  end

  # POST /credit_cards
  # POST /credit_cards.json
  def create
    @credit_card = CreditCard.new(credit_card_params)
    if @credit_card.save
      redirect_to '/wallet', notice: 'Credit card was successfully created.' 
    else
      render :new 
    end
  end

  
  def destroy
    @credit_card.destroy
    redirect_to '/wallet', notice: 'Credit card was successfully destroyed.' 
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_credit_card
      @credit_card = CreditCard.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def credit_card_params
      params.require(:credit_card).permit(:number, :cvv, :expiration_date)
    end
end
